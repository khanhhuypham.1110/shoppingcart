import { Product } from "../models/product.js"
import { editData, deleteData } from "./main.js";
import { addProductToCart, removeProductFromCart, deleteCartItem } from "../services/service.js"


export function renderProductList(list, elementId) {
    let contentHTML = "";
    let category = document.getElementById('category').value
    list.forEach((item) => {
        if (category.toLowerCase() === item.type.toLowerCase()) {
            contentHTML += `
            <div class="item">
                    <div class="item-icon">
                        <i class="fa-brands fa-apple"></i>
                        <span>in stock</span>
                    </div>
                    <div class="item-img">
                        <img class="card-img-top" src="${item.img}" alt="${item.name}">
                        <p>$ ${item.price}</p>
                    </div>
                    <div class="item-body">
                        <div class="item-line">
                            <p>${item.name}</p>
                            <i class="fa-solid fa-heart"></i>
                        </div>
                        <div class="wrapper">
                            <p><b>${item.screen}, ${item.frontCamera} ,${item.backCamera}</b></p>
                            <p class="item-infor">${item.desc}</p>
                            <div class="item-purchase">
                                <button onclick="addProductToCart('${item.id}')"><span class="mr-3">Add to cart</span><i
                                        class="fa-solid fa-cart-plus"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            `
        }
    })
    document.getElementById(elementId).innerHTML = contentHTML
}


export function renderCart(list, elementId) {
    let contentHTML = ""
    list.forEach((p) => {
        contentHTML += `
        <div class="cart-item grid grid-cols-7 gap-4">
            <div class="col-span-2">
                <img class="cart-image" src="${p.img}" alt="${p.name}">
            </div>
            <div>${p.name}h</div>
            <div class="col-span-2">
                <button onclick="addProductToCart('${p.id}')"><i class="fa-solid fa-plus"></i></button>
                <input class="idRepresention" type="hidden" value ="${p.id}">
                <span>${p.getQuantity()}</span>
                <button onclick="removeProductFromCart('${p.id}')"><i class="fa-solid fa-minus"></i></button>
            </div>
            <div>
                <span>$</span><span>${p.price}</span>
            </div>
            <div>
                <button onclick = "deleteCartItem('${p.id}')">
                    <i class="fa-solid fa-trash"></i>
                </button>
            </div>
        </div>
        `
    })
    document.getElementById(elementId).innerHTML = contentHTML
}


export function renderProductListForAdmin(list, elementId) {
    let contentHTML = ''
    list.forEach((item) => {
        contentHTML +=
            `
        <tr>
            <td>${item.id}</td>
            <td>${item.name}</td>
            <td>${item.price}</td>
            <td>${item.stock}</td>
            <td><img src="${item.img}" alt="" width="120px" height="120px"/></td>
            <td>${item.status}</td>
            <td>${item.desc}</td>
            <td class="d-flex justify-content-between">
                <button class="btn btn-success mr-2" onclick="editData('${item.id}')"><i class="fa-sharp fa-solid fa-pen-to-square"></i></button>
                <button class="btn btn-danger" onclick="deleteData('${item.id}')"><i class="fa-solid fa-trash"></i></button>
            </td>
        </tr>
    `
    })
    document.getElementById(elementId).innerHTML = contentHTML
}


export function getInputForm() {
    let id = document.getElementById('id').value
    let name = document.getElementById('name').value
    let price = document.getElementById('price').value
    let screen = document.getElementById('screen').value
    let backCamera = document.getElementById('backCamera').value
    let frontCamera = document.getElementById('frontCamera').value
    let img = document.getElementById('image').value
    let desc = document.getElementById('desc').value
    let type = document.getElementById('type').value
    let stock = document.getElementById('stock').value
    let product = new Product(name, price, screen, backCamera, frontCamera, img, desc, type, stock)
    // product.setId()
    return product
}




