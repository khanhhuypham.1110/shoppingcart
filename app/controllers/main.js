import { renderProductList, renderCart, renderProductListForAdmin, getInputForm } from "./controller.js"
import { Product } from "../models/product.js"
import { CartItem } from "../models/cartItem.js"
import { Cart } from "../models/cart.js"

export let cart = new Cart(new Array())
export let productList = [];


let idForUpdate;
const BASE_URL = "https://635f4b17ca0fe3c21a991d30.mockapi.io"
window.fetchData = fetchData
window.addData = addData
window.editData = editData
window.updateData = updateData
window.deleteData = deleteData
window.turnOffInputForm = turnOffInputForm
window.turnOnInputForm = turnOnInputForm


function fetchData() {
    console.log("Yes");
    axios({
        url: BASE_URL + "/QLSP",
        method: "GET",
    }).then(
        function (res) {
            let path = window.location.pathname.split('/').pop().toLowerCase();
            switch (path) {
                case 'admin.html': {
                    productList = [];
                    res.data.forEach((data) => {
                        let product = new Product(data.name, data.price, data.screen, data.backCamera, data.frontCamera, data.img, data.desc, data.type, data.stock)
                        product.setId(data.id)
                        productList.push(product)
                    })
                    renderProductListForAdmin(productList, "tbody")
                }

                default: {
                    productList = [];
                    res.data.forEach((data) => {
                        let product = new Product(data.name, data.price, data.screen, data.backCamera, data.frontCamera, data.img, data.desc, data.type, data.stock)
                        product.setId(data.id)
                        productList.push(product)
                    })
                    let myJSON = getLocalStorage()
                    if (myJSON != null) {
                        let arr = new Array()
                        myJSON.forEach((data) => {
                            let product = new Product(data.name, data.price, data.screen, data.backCamera, data.frontCamera, data.img, data.desc, data.type, data.stock)
                            product.setId(data.id)
                            let cartItem = new CartItem(product, data.quantity)
                            arr.push(cartItem)
                        })
                        cart = new Cart(arr)
                    }
                    renderProductList(productList, "productList")
                    document.getElementById('numberOfCartItem').innerHTML = cart.getItemQuanity()
                }
            }
        }
    ).catch(
        function (err) {
            console.log(err);
        }
    )
}
fetchData()

export function addData() {
    let newProduct = getInputForm()
    newProduct.setId(newProduct.id)
    if (newProduct != null) {
        axios({
            method: 'post',
            url: `${BASE_URL}/QLSP`,
            data: newProduct
        }).then((res) => {
            console.log(res.data);
            fetchData()
        }).catch((err) => {
            console.log(err);
        })
    }
}

export function deleteData(id) {
    axios({
        method: "DELETE",
        url: `${BASE_URL}/QLSP/${id}`
    }).then((res) => {
        console.log(res.data);
        fetchData()
    }).catch((err) => {
        console.log(err);
    })
}

export function editData(id) {
    axios({
        method: "get",
        url: `${BASE_URL}/QLSP/${id}`
    }).then((res) => {
        turnOnInputForm()
        document.getElementById('idField').style.display = "block"
        document.getElementById('add-btn').style.display = "none"
        document.getElementById('update-btn').style.display = "block"
        document.querySelector(".input-form-title").innerHTML = "Product Information"
        let data = res.data
        document.getElementById('id').value = data.id
        document.getElementById('name').value = data.name
        document.getElementById('price').value = data.price
        document.getElementById('screen').value = data.screen
        document.getElementById('backCamera').value = data.backCamera
        document.getElementById('frontCamera').value = data.frontCamera
        document.getElementById('image').value = data.img
        document.getElementById('desc').value = data.desc
        document.getElementById('type').value = data.type
        document.getElementById('stock').value = data.stock
        idForUpdate = data.id

    }).catch((err) => {
        console.log(err);
    })
}

export function updateData() {
    let updatingData = getInputForm()

    console.log(`${updatingData.getId()}`);
    // console.log(`${BASE_URL}/QLSP/${updateData.id}`); tại sao updateData.id = undefined
    console.log(updatingData);
    console.log(`${BASE_URL}/QLSP/${idForUpdate}`);
    axios({
        method: "PUT",
        url: `${BASE_URL}/QLSP/${idForUpdate}`,
        data: updatingData,
    }).then((res) => {
        console.log(res.data);
        fetchData()
    }).catch((err) => {
        console.log(err);
    })
}

export function turnOnInputForm() {
    document.getElementById('idField').style.display = "none"
    document.getElementById('input-form').style.display = "block"
    document.getElementById('update-btn').style.display = "none"
    document.getElementById('add-btn').style.display = "block"
    document.querySelector(".input-form-title").innerHTML = "Add New Product"
}

export function turnOffInputForm() {
    document.getElementById('input-form').style.display = "none"
}

export function saveLocalStorage(list) {
    let myJSON = JSON.stringify(list)
    localStorage.setItem("cartList", myJSON)
}

function getLocalStorage() {
    let myJSON = localStorage.getItem("cartList")
    return JSON.parse(myJSON);
}


