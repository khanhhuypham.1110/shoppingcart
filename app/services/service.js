import { cart, productList, saveLocalStorage } from "../controllers/main.js"
import { renderCart } from "../controllers/controller.js"


window.addProductToCart = addProductToCart
window.removeProductFromCart = removeProductFromCart
window.turnOnCartList = turnOnCartList
window.turnOffCartList = turnOffCartList
window.deleteCartItem = deleteCartItem
window.clearCartList = clearCartList

export function addProductToCart(id) {
    let product = searchProductById(id)
    cart.addToCart(product)
    saveLocalStorage(cart.list)
    calculateTotalBill()
    renderCart(cart.list, "cart-content")
    document.getElementById('numberOfCartItem').innerHTML = cart.getItemQuanity()
    getItemQuanity()
}


export function removeProductFromCart(id) {
    let product = searchProductById(id)
    cart.removeFromCart(product)
    saveLocalStorage(cart.list)
    calculateTotalBill()
    renderCart(cart.list, "cart-content")
    document.getElementById('numberOfCartItem').innerHTML = cart.getItemQuanity()
    getItemQuanity()
}

export function deleteCartItem(id) {
    cart.deleteCartItem(id)
    saveLocalStorage(cart.list)
    calculateTotalBill()
    renderCart(cart.list, "cart-content")
    fetchData()
}


function turnOnCartList() {
    document.querySelectorAll(".cart-wrapper")[0].style.display = "block";
    renderCart(cart.list, "cart-content")
    calculateTotalBill()
}
function turnOffCartList() {
    document.querySelectorAll(".cart-wrapper")[0].style.display = "none";

}
export function calculateTotalBill() {
    let totalBill = cart.calculateTotalBill()
    document.getElementById("totalBill").innerHTML = totalBill
}


export function clearCartList() {
    cart.clearCart()
    saveLocalStorage(cart.list)
    calculateTotalBill()
    renderCart(cart.list, "cart-content");
    fetchData()
}


function searchProductById(id) {
    let product;
    if (productList != null) {
        productList.forEach((item) => {
            if (item.id == id) {
                product = item
            }
        })
    }
    return product
}

function getItemQuanity() {
    let inputTagList = document.getElementsByClassName("idRepresention")
    // console.log(inputTagList);

    for (let j = 0; j < cart.list.length; j++) {
        for (let i = 0; i < inputTagList.length; i++) {
            // console.log(inputTagList[i].parentNode.lastElementChild.previousElementSibling);
            if (cart.list[j].id == inputTagList[i].value) {
                inputTagList[i].parentNode.lastElementChild.previousElementSibling.innerHTML = cart.list[j].quantity
            }
        }
    }
}



