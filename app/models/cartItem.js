import { Product } from "./product.js";


export class CartItem extends Product {
    constructor(product, quantity = 1) {
        super(product.name, product.price, product.screen, product.backCamera, product.frontCamera, product.img, product.desc, product.type, product.stock)
        super.setId(product.id)
        this.quantity = quantity
    }
    getQuantity() {
        return this.quantity
    }
}