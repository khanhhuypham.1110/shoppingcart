const data = [
    {
        "id": "L4pgv",
        "name": "iphoneX",
        "price": "1000",
        "screen": "screen 68",
        "backCamera": "2 camera 12 MP",
        "frontCamera": "7 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/114115/iphone-x-64gb-hh-600x600.jpg",
        "desc": "Thiết kế mang tính đột phá",
        "type": "iphone",
        "stock": "12"

    },
    {
        "id": "ct9s1",
        "name": "Samsung Galaxy M51 ",
        "price": "3500",
        "screen": "screen 69",
        "backCamera": " Chính 64 MP & Phụ 12 MP, 5 MP, 5 MP",
        "frontCamera": " 32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/217536/samsung-galaxy-m51-trang-new-600x600-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "Samsung",
        "stock": "12"
    },
    {
        "id": "aT6nh",
        "name": "Samsung Galaxy M22",
        "price": "4500",
        "screen": "screen 70",
        "backCamera": "Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": " 32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/217536/samsung-galaxy-m51-trang-new-600x600-600x600.jpg",
        "desc": "Thiết kế mang tính đột phá",
        "type": "Samsung",
        "stock": "12"
    },
    {
        "id": "1V5pT",
        "name": "Iphone 11",
        "price": "1000",
        "screen": "screen 54",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://didongviet.vn/pub/media/catalog/product//i/p/iphone-11-pro-max-256gb-didongviet_23.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "Iphone",
        "stock": "12"
    },

    {
        "id": "aaI2y",
        "name": "Iphone 13",
        "price": "2000",
        "screen": "screen 70",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/250261/iphone-13-pro-max-xanh-la-thumb-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "Iphone",
        "stock": "12"
    },


    {
        "id": "Vnph5",
        "name": "Iphone 14",
        "price": "2000",
        "screen": "screen 70",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/240259/iPhone-14-thumb-tim-1-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "Iphone",
        "stock": "12"
    },

    {
        "id": "Z7ZbT",
        "name": "Samsung Galaxy S22 Ultra 5G",
        "price": "2000",
        "screen": "screen 70",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/285696/samsung-galaxy-z-flip4-5g-512gb-thumb-xanh-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "samsung",
        "stock": "12"
    },
    {
        "id": "fCedT",
        "name": "Samsung Galaxy Z Flip4 5G",
        "price": "2000",
        "screen": "screen 70",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/235838/Galaxy-S22-Ultra-Burgundy-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "samsung",
        "stock": "12"
    },

    {
        "id": "ZfR8k",
        "name": "OPPO Reno6 series",
        "price": "450",
        "screen": "screen 70",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/260546/oppo-reno8-pro-thumb-xanh-1-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "oppo",
        "stock": "12"
    },

    {
        "id": "za2C5",
        "name": "OPPO Find X5 Pro 5G",
        "price": "1200",
        "screen": "screen 70",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/250622/oppo-find-x5-pro-den-thumb-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "oppo",
        "stock": "12"
    },


    {
        "id": "FsikL",
        "name": "OPPO Reno8 series",
        "price": "860",
        "screen": "screen 70",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/236186/oppo-reno6-5g-black-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "oppo",
        "stock": "12"
    }


    , {
        "id": "3nIWh",
        "name": "Realme 9i",
        "price": "200",
        "screen": "screen 70",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/275732/realme-9i-den-thumb-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "Realme",
        "stock": "12"
    }

    , {
        "id": "dsKm0",
        "name": "Realme 9",
        "price": "240",
        "screen": "screen 70",
        "backCamera": "Camera: Chính 12 MP & Phụ 64 MP, 12 MP",
        "frontCamera": "32 MP",
        "img": "https://cdn.tgdd.vn/Products/Images/42/275632/realme-9-4g-vang-thumb-1-600x600.jpg",
        "desc": "Thiết kế đột phá, màn hình tuyệt đỉnh",
        "type": "Realme",
        "stock": "12"
    }

]