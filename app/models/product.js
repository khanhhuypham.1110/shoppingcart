export class Product {
    constructor(name, price, screen, backCamera, frontCamera, img, desc, type, stock) {
        this.id = this.randomId()
        this.name = name
        this.price = price
        this.screen = screen
        this.backCamera = backCamera
        this.frontCamera = frontCamera
        this.img = img
        this.desc = desc
        this.type = type
        this.stock = stock
    }

    setId(id) {
        this.id = id
    }

    getId() {
        return this.id
    }

    randomId() {
        const length = 5
        var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

        if (!length) {
            length = Math.floor(Math.random() * chars.length);
        }

        var str = '';
        for (var i = 0; i < length; i++) {
            str += chars[Math.floor(Math.random() * chars.length)];
        }
        return str;
    }

    searchProductById(id, list) {
        let product;
        if (list != null) {
            List.forEach((p) => {
                if (p.id === id) {
                    product = p
                }
            })
        }
        return product
    }
    removeImgBackground() { }



}
