import { CartItem } from "./cartItem.js"

export class Cart {
    constructor(itemList) {
        this.list = itemList
    }
    searchCartItemById(id) {
        let item;
        if (this.list != null) {
            this.list.forEach((i) => {
                if (i.id == id) {
                    item = i
                }
            })
        }
        return item
    }

    searchItemIndexById(id) {
        let index;
        if (this.list != null) {
            for (let i = 0; i < this.list.length; i++) {
                if (this.list[i].id == id) {
                    index = i
                }
            }
        }
        return index
    }
    checkExistingItem(id) {
        let isExisting = false;
        if (this.list != null) {
            for (let i = 0; i < this.list.length; i++) {
                if (this.list[i].id === id) {
                    isExisting = true
                }
            }
        }
        return isExisting
    }

    addToCart(item) {
        if (this.checkExistingItem(item.id)) {
            let cartItem = this.searchCartItemById(item.id)
            cartItem.quantity += 1
        } else {
            let cartItem = new CartItem(item, 1)
            this.list.push(cartItem)
        }
    }
    getItemQuanity() {
        let totalItemQuantity = 0;
        if (this.list != null) {
            for (let i = 0; i < this.list.length; i++) {
                totalItemQuantity += this.list[i].quantity
            }
        }
        return totalItemQuantity
    }

    removeFromCart(item) {
        if (this.checkExistingItem(item.id)) {
            let cartItem = this.searchCartItemById(item.id)
            if (cartItem.quantity > 1) {
                cartItem.quantity -= 1
            } else {
                this.deleteCartItem(item.id)
            }
        }
        else {
            this.deleteCartItem(item.id)
        }
    }

    deleteCartItem(id) {
        let index = this.searchItemIndexById(id)
        this.list.splice(index, 1)
    }

    clearCart() {
        this.list.splice(0, this.list.length)
    }

    calculateTotalBill() {
        let totalBill = 0;
        this.list.forEach((item) => {
            totalBill += item.price * item.quantity
        })
        return totalBill
    }
}

